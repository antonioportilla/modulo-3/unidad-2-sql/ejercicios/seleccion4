<?php

namespace app\controllers;
use yii\data\SqlDataProvider;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use app\models\Equipo;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /*
     * Creado esta accion
     */
    
    public function actionCrud(){
        return $this->render('gestion');
    }


     public function actionConsulta1a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            
            'query'=> Ciclista::find()->join('inner join', 'etapa','ciclista.dorsal=etapa.dorsal')->distinct()->select("nombre, edad")
                ,
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas.",
            "sql"=>"SELECT DISTINCT c.nombre, c.edad FROM etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal",
        ]);
    }
    
     public function actionConsulta1(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT c.nombre, c.edad FROM etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT c.nombre, c.edad FROM etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas.",
            "sql"=>"SELECT DISTINCT c.nombre, c.edad FROM etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal",
        ]);


     }

    
     public function actionConsulta2a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            
            'query'=> Ciclista::find()->innerjoin( 'puerto','ciclista.dorsal=puerto.dorsal')->distinct()->select("nombre, edad")
                 ,
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        // $a=Ciclista::find()->innerJoinWith( 'puertos',true,'inner join')->all();
        // var_dump($a[0]->puertos); // innerJoinWith utiliza los modelos que ya est´n relacionados 
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 2",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos.",
            "sql"=>"SELECT DISTINCT c.nombre, c.edad FROM puerto p INNER JOIN ciclista c ON c.dorsal=p.dorsal",
        ]);
    }
    
     public function actionConsulta2(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT c.nombre, c.edad FROM puerto p INNER JOIN ciclista c ON c.dorsal=p.dorsal) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT c.nombre, c.edad FROM etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 2",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos.",
            "sql"=>"SELECT DISTINCT c.nombre, c.edad FROM puerto p INNER JOIN ciclista c ON c.dorsal=p.dorsal",
        ]);


     }
     
     
          public function actionConsulta3a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            
            'query'=> Ciclista::find()->innerjoin( 'etapa','ciclista.dorsal=etapa.dorsal')->innerJoin('puerto','ciclista.dorsal=puerto.dorsal')->distinct()->select("nombre, edad")
                 ,
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        // $a=Ciclista::find()->innerJoinWith( 'puertos',true,'inner join')->all();
        // var_dump($a[0]->puertos); // innerJoinWith utiliza los modelos que ya est´n relacionados 
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos.",
            "sql"=>"SELECT DISTINCT c.nombre, c.edad FROM (etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal) INNER JOIN puerto p ON c.dorsal=p.dorsal",
        ]);
    }
    
     public function actionConsulta3(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT c.nombre, c.edad FROM (etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal) INNER JOIN puerto p ON c.dorsal=p.dorsal) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT c.nombre, c.edad FROM (etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal) INNER JOIN puerto p ON c.dorsal=p.dorsal" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos.",
            "sql"=>"SELECT DISTINCT c.nombre, c.edad FROM (etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal) INNER JOIN puerto p ON c.dorsal=p.dorsal",
        ]);


     }
    
    
    public function actionConsulta4a(){
        
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Equipo::find()->innerJoinWith('ciclistas',true)->innerJoin('etapa','etapa.dorsal=ciclista.dorsal')->distinct()->select("director")
                         ,
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa.",
            "sql"=>"SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) ON equipo.nomequipo=ciclista.nomequipo",
        ]);
    }
    
     public function actionConsulta4(){
            // mediante DAO
        
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) ON equipo.nomequipo=ciclista.nomequipo" ,
        
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa.",
            "sql"=>"SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) ON equipo.nomequipo=ciclista.nomequipo",
        ]);
    }
    
    public function actionConsulta5a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->innerJoinWith ('llevas',true)->distinct()->select("ciclista.dorsal,nombre")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 5",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algín maillot.",
            "sql"=>"SELECT DISTINCT lleva.dorsal, ciclista.nombre FROM ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal",
        ]);
    }
    
     public function actionConsulta5(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT lleva.dorsal, ciclista.nombre FROM ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT lleva.dorsal, ciclista.nombre FROM ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 5",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algín maillot.",
            "sql"=>"SELECT DISTINCT lleva.dorsal, ciclista.nombre FROM ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal",
        ]);
    }
    
    
        public function actionConsulta6a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->innerJoin ('lleva','ciclista.dorsal=lleva.dorsal')->innerJoin ('maillot','lleva.código=maillot.código')->distinct()->select("ciclista.dorsal,ciclista.nombre")->where("maillot.color='amarillo'")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 6",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algún maillot amarillo.",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM maillot INNER JOIN (ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal) ON maillot.código=lleva.código WHERE (((maillot.color)='amarillo'))",
        ]);
    }
    
     public function actionConsulta6(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM maillot INNER JOIN (ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal) ON maillot.código=lleva.código WHERE (((maillot.color)='amarillo'))) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM maillot INNER JOIN (ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal) ON maillot.código=lleva.código WHERE (((maillot.color)='amarillo'))" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 6",
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algún maillot amarillo.",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM maillot INNER JOIN (ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal) ON maillot.código=lleva.código WHERE (((maillot.color)='amarillo'))",
        ]);
    }
    
            public function actionConsulta7a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->innerJoinWith ('etapas',true)->innerJoinWith ('llevas',true)->distinct()->select("ciclista.dorsal")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 7",
            "enunciado"=>"Dorsal de los ciclistas que hayan llevado algún maillot que han ganado etapas.",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal",
        ]);
    }
    
     public function actionConsulta7(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT ciclista.dorsal FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT ciclista.dorsal FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 7",
            "enunciado"=>"Dorsal de los ciclistas que hayan llevado algún maillot que han ganado etapas.",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal",
        ]);
    }
    
    public function actionConsulta8a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->distinct()->select("numetapa")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"Indicar el numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT puerto.numetapa FROM puerto",
        ]);
    }
    
     public function actionConsulta8(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT puerto.numetapa FROM puerto) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT puerto.numetapa FROM puerto" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"Indicar el numetapa de las etapas que tengan puertos",
            "sql"=>"SELECT DISTINCT puerto.numetapa FROM puerto",
        ]);
    } 
    
        public function actionConsulta9a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->innerJoin('etapa','ciclista.dorsal=etapa.dorsal')->innerJoin('puerto','etapa.numetapa=puerto.numetapa')->distinct()->select("etapa.kms km,etapa.numetapa numetapa0")->where("ciclista.nomequipo='banesto'")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['km','numetapa0'],
            "titulo"=>"Consulta 9",
            "enunciado"=>"Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos.",
            "sql"=>"SELECT DISTINCT etapa.kms, etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto'",
        ]);
    }
    
     public function actionConsulta9(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT etapa.kms, etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto') c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT etapa.kms, etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto'" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['kms','numetapa'],
            "titulo"=>"Consulta 9",
            "enunciado"=>"Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos.",
            "sql"=>"SELECT DISTINCT etapa.kms, etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto'",
        ]);
    } 
 
    
         public function actionConsulta10a(){
        // mediante active record
             $subconsulta=Etapa::find()->innerJoinWith('puertos',true)->distinct()->select('etapa.dorsal');
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()->innerJoinWith('puertos',true)->distinct()->select('count(distinct etapa.dorsal) total'),
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
        
        
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el número de ciclists que hayan ganado alguna etapa con puerto.",
            "sql"=>"SELECT COUNT(*) FROM (SELECT DISTINCT etapa.dorsal FROM etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa) c1",
        ]);
    }
    
     public function actionConsulta10(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT 1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT COUNT(*) AS total FROM (SELECT DISTINCT etapa.dorsal FROM etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa) c1" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el número de ciclists que hayan ganado alguna etapa con puerto.",
            "sql"=>"SELECT COUNT(*) FROM (SELECT DISTINCT etapa.dorsal FROM etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa) c1",
        ]);
    } 
  
             public function actionConsulta11a(){
        // mediante active record
             
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->innerJoinWith('puertos',true)->select("puerto.nompuerto")->where("ciclista.nomequipo='banesto'")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 11",
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por los ciclistas de Banesto.",
            "sql"=>"SELECT puerto.nompuerto FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE ciclista.nomequipo='banesto'",
        ]);
    }
    
     public function actionConsulta11(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(*) from (SELECT puerto.nompuerto FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE ciclista.nomequipo='banesto')c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT puerto.nompuerto FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE ciclista.nomequipo='banesto'" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 11",
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por los ciclistas de Banesto.",
            "sql"=>"SELECT puerto.nompuerto FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE ciclista.nomequipo='banesto'",
        ]);
    } 
    
    
    
    public function actionConsulta12a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->innerJoinWith('etapas',true)->innerJoinWith('puertos',true)->distinct()->select("etapa.numetapa as num")->where("ciclista.nomequipo='banesto' AND etapa.kms>=200")
                ,
            'pagination'=>[
                'pageSize'=>3,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['num'],
            "titulo"=>"Consulta 12",
            "enunciado"=>"Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con mas de 200 Km.",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto' AND etapa.kms>=200",
        ]);
    }
    
     public function actionConsulta12(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(*) from (SELECT DISTINCT etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto' AND etapa.kms>=200)c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto' AND etapa.kms>=200" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>3,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 12",
            "enunciado"=>"Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con mas de 200 Km.",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto' AND etapa.kms>=200",
        ]);
    } 
    
     
    
    
      public function actionConsulta12b(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->innerJoinWith('etapas',true)->innerJoinWith('puertos',true)->distinct()->select("etapa.numetapa as num")->where("ciclista.nomequipo='banesto' AND etapa.kms>=200"),
            'pagination'=>array (
                'pageSize'=>6,
            ),
        ]);
        
        return $this->render("listView",[
           "dataProvider" => $dataProvider,
        ]);
        
        
    }
    
     }

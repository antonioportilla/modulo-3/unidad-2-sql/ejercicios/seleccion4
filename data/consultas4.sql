﻿USE ciclistas;

-- 1.1 Nombre y edad de los ciclistas que han ganado etapas.
SELECT DISTINCT c.nombre, c.edad FROM etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal;

-- 1.2 Nombre y edad de los ciclistas que han ganado puertos.
SELECT DISTINCT c.nombre, c.edad FROM puerto p INNER JOIN ciclista c ON c.dorsal=p.dorsal;

-- 1.3 Nombre y edad de los ciclistas que han ganado etapas y puertos.
SELECT DISTINCT c.nombre, c.edad FROM (etapa e INNER JOIN ciclista c ON c.dorsal=e.dorsal) INNER JOIN puerto p ON c.dorsal=p.dorsal;

-- 1.4 Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa.
SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) ON equipo.nomequipo=ciclista.nomequipo;

-- 1.5 Dorsal y nombre de los ciclistas que hayan llevado algín maillot.
SELECT DISTINCT lleva.dorsal, ciclista.nombre FROM ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal;

-- 1.6 Dorsal y nombre de los ciclistas que hayan llevado algún maillot amarillo.
SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM maillot INNER JOIN (ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal) ON maillot.código=lleva.código WHERE (((maillot.color)='amarillo'));

-- 1.7 Dorsal de los ciclistas que hayan llevado algún maillot que han ganado etapas.
SELECT DISTINCT ciclista.dorsal FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal;

-- 1.8 Indicar el numetapa de las etapas que tengan puertos.
SELECT DISTINCT puerto.numetapa FROM puerto;
SELECT COUNT(*) cuenta_numetapa FROM (SELECT DISTINCT puerto.numetapa FROM puerto) c1;

-- 1.9 Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos.
  SELECT DISTINCT etapa.kms, etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto';

-- 1.10 Listar el número de ciclists que hayan ganado alguna etapa con puerto.

SELECT COUNT(*) FROM (SELECT DISTINCT etapa.dorsal FROM etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa) c1;

-- 1.11 Indicar el nombre de los puertos que hayan sido ganados por los ciclistas de Banesto.

SELECT puerto.nompuerto FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE ciclista.nomequipo='banesto';

-- 1.12 Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con mas de 200 Km.
SELECT DISTINCT etapa.numetapa FROM (ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal) INNER JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE ciclista.nomequipo='banesto' AND etapa.kms>=200;